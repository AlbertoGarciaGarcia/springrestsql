package crudIPiezas.Service;

import java.util.List;

import crudIPiezas.Dto.Suministra;

public interface ISuministraService {
	public List<Suministra> listarSuministra();
	
	public Suministra guardarSuministra(Suministra Suministra);	
	
	public Suministra suministraXID(int id); 

	public Suministra actualizarSuministra(Suministra Suministra); 
	
	public void eliminarSuministra(int id);
}
